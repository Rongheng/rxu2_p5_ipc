import 'package:flutter/material.dart';

class UserProfile extends StatelessWidget {
  final String name;
  final String phone;
  final DateTime birthDate;
  final String gender;

  UserProfile(
      {required this.name,
      required this.phone,
      required this.birthDate,
      required this.gender});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(name),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            CircleAvatar(
              radius: 50,
              child: Text(name[0]),
            ),
            const SizedBox(height: 16),
            Text(
              name,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            Text(
              "Nació el ${birthDate.toLocal()}".split(' ')[0],
              style: const TextStyle(fontSize: 16),
            ),
            Text(
              "Género: $gender",
              style: const TextStyle(fontSize: 16),
            ),
            Text(
              phone,
              style: const TextStyle(fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }
}
