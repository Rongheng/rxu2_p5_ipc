import 'package:flutter/material.dart';
import 'UserForm.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        // Verifica el ancho disponible en la pantalla
        final maxWidth = constraints.maxWidth;

        // Decide el ancho máximo para el formulario
        final formMaxWidth = maxWidth > 600 ? 600.0 : maxWidth.toDouble();

        return MaterialApp(
          title: 'Formulario de Datos',
          theme: ThemeData(
            primarySwatch: Colors.orange,
          ),
          home: Scaffold(
            appBar: AppBar(
              title: const Text('Formulario de Datos'),
            ),
            body: Center(
              child: Container(
                width: formMaxWidth,
                padding: const EdgeInsets.all(16.0),
                child: UserForm(),
              ),
            ),
          ),
        );
      },
    );
  }
}
